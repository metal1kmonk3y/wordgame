/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var express = require('express');
var session = require('express-session');
var mongoose = require('mongoose');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var admins = require('./routes/admins');

var app = express();
var favicon = require('serve-favicon');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session(
    {
        secret:'A SECRET KEY. SHOULD BE UNIQUE TO THE APP. DONT EVER SHOW IT TO ANYONE, EVERY. GOT IT!',
        resave : true,
        saveUninitialized : true
    }
));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res, next) {
    res.sendFile( 'index.html', { root : __dirname + "/public" } );
});

app.get('/wordgame', function(req, res) {
    res.sendFile( 'index.html', { root : __dirname + "/public" } );
 });

app.use('/wordgame/api/v3/admins', admins);
app.use('/wordgame/api/v3', routes);

connect()
    .on( 'error', console.log )
    .on( 'disconneted', connect );

function connect() {
    var options = { server : { socketOptions : { keepAlive : 1 } } };
    return mongoose.connect( 'mongodb://localhost:27017/wordGame', options ).connection;
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send( { msg : err.message } );
    });
}


// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send( { msg: err.message } );
} );


module.exports = app;
