/*
 * CS402-HW3
 *
 * @Author Prasanna Shiwakoti
 * @Modified 3/10/2017
 */

var font, level, colors, allFonts, allLevels, game, user, editingUserId;
var creatingUser = false, editingMetadata = false, editingUser = false;
var editMode = "defaults";

/*************************************************/
/* Admins */
/*************************************************/

/* create new user */
var  createUser = function (evt) {
    evt.preventDefault();
    $("#inputFirstName").val("");
    $("#inputLastName").val("");
    $("#inputEmail").val("");
    $("#inputPassword").val("");
    $("#optionsRole1, #optionsRole2").attr("disabled",false);

    $('input:radio[name=optionsRole][value=user]').click();
    $('input:radio[name=optionsEnabled][value=true]').click();

    creatingUser = true;
    editingUser =  true;
    setPage();
};

/* create a new user */
var  createNewUser = function (evt) {
    evt.preventDefault();
    var firstName = $("#inputFirstName").val();
    var lastName = $("#inputLastName").val();
    var email = $("#inputEmail").val();
    var password = $("#inputPassword").val();
    var role = $(".role-radio :checked").val();
    var enabled = $(".enabled-radio :checked").val();

    $.ajax({
        url : "/wordgame/api/v3/admins/user",
        method: "POST",
        processData: false,
        contentType: 'application/json',
        data: JSON.stringify({
            name :{
                first: firstName,
                last: lastName
            },
            email: email,
            password: password,
            enabled: enabled,
            role: role
        }),
        success: backToUserList
    });
};

/* cancel edit user */
var  updateUser = function (uid) {
    editingUser = true;
    creatingUser = false;
    setPage();

    $.ajax({
        url: "/wordgame/api/v3/admins/user/" + uid,
        method: "GET",
        success: editUser
    });
};

var editUser = function (data) {
    var userBeingEdited = data;
    editingUserId = userBeingEdited._id;

    $("#inputFirstName").val(userBeingEdited.name.first);
    $("#inputLastName").val(userBeingEdited.name.last);
    $("#inputEmail").val(userBeingEdited.email);

    $('input:radio[name=optionsRole][value=' +userBeingEdited.role+']').click();

    if(userBeingEdited.enabled){
        $('input:radio[name=optionsEnabled][value=true]').click();
    } else{
        $('input:radio[name=optionsEnabled][value=false]').click();
    }

    if(userBeingEdited._id == user._id){
        $("#optionsRole1, #optionsRole2").attr("disabled",true);
    } else{
        $("#optionsRole1, #optionsRole2").attr("disabled",false);
    }
};

/* cancel edit user */
var sendUserUpdate = function (evt) {
    evt.preventDefault();
    editingUser = false;
    creatingUser = false;

    var firstName = $("#inputFirstName").val();
    var lastName = $("#inputLastName").val();
    var email = $("#inputEmail").val();
    var password = $("#inputPassword").val();
    var role = $(".role-radio :checked").val();
    var enabled = $(".enabled-radio :checked").val();

    $.ajax({
        url: "/wordgame/api/v3/admins/user/" + editingUserId,
        method: "PUT",
        processData: false,
        contentType: 'application/json',
        data: JSON.stringify({
            name :{
                first: firstName,
                last: lastName
            },
            email: email,
            password: password,
            enabled: enabled,
            role: role
        }),
        success: setPage
    });
};

/* cancel edit user */
var  cancelCreateUser = function (evt) {
    evt.preventDefault();
    editingUser = false;
    creatingUser = false;
    setPage();
};

var backToUserList = function (data) {
    editingUser = false;
    creatingUser = false;
    setPage();
};

var getUserList = function () {
    $.ajax({
        url: "/wordgame/api/v3/admins/users",
        method: "GET",
        success: displayUsers
    });
};

var displayUsers = function (data) {

    var allUsers = data;

    $("#current-users-list").children().remove();

    for (var  i = 0; i <  allUsers.length; i++){

        var tableLeft = "<tr onclick=\"updateUser(id)\" id=\"" + allUsers[i]._id + "\""+ ">";
        var tableRight = "</tr>";

        $("#current-users-list").append($(tableLeft + "<td>" +
            allUsers[i].name.first + "</td><td>" +
            allUsers[i].name.last  + "</td><td>" +
            allUsers[i].email +  "</td><td>" +
            allUsers[i].role +  "</td><td>" +
            allUsers[i].enabled  +
            "</td>" + tableRight
        ));
    }
};

var editMetadata = function (evt) {
    evt.preventDefault();
    editingMetadata = true;
    setPage();
    getDefaults();
};

var editDefaults = function (evt) {
    evt.preventDefault();
    editMode = "defaults";
    setPage();
    getDefaults();
};


var getDefaults = function (){

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/defaults",
        method: "GET",
        success: displayDefaults
    });
};

var displayDefaults = function (data) {
    allFonts = data.fonts;
    allLevels = data.levels;
    var editableMetas = data.defaults;
    level = editableMetas.level;
    font = editableMetas.font;
    colors = editableMetas.colors;

    $("#default-level-picker").children().remove();
    $("#default-font-picker").children().remove();

    /* setting all colors */
    $("#default-word-color").val(colors.wordBackground);
    $("#default-guess-color").val(colors.guessBackground);
    $("#default-fore-color").val(colors.textColor);

    allLevels.forEach(function (level) {
        $("#default-level-picker").append($("<option></option>").text(level.name));
    });

    /* setting level to default */
    $("#default-level-picker").val(level.name);

    /* adding all fonts */
    allFonts.forEach(function (font) {
        //$("meta").after($("<link rel=\"stylesheet\" type=\"text/css\"/>").attr("href", font.url));
        $("#default-font-picker").append($("<option></option>").text(font.family));
    });

    /* setting font to default */
    $("#default-font-picker").val(font.family);
};

var updateServerDefaults = function (evt) {
    evt.preventDefault();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/defaults",
        method: "PUT",
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            colors: colors,
            level: level,
            font: font
        })

    });
};

var editFonts = function () {
    editMode = "fonts";
    setPage();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/fonts",
        method: "GET",
        success: displayFonts
    });
};

var addFont = function (evt) {
    evt.preventDefault();

    var newFont = {};
    newFont.family = $("#inputFamily").val();
    newFont.category = $("#inputCategory").val();
    newFont.rule = $("#inputRule").val();
    newFont.url = $("#inputUrl").val();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/fonts/font",
        method: "POST",
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            category: newFont.category,
            family: newFont.family,
            rule: newFont.rule,
            url: newFont.url
        }),
        success:editFonts
    });
};

var deleteFont = function (fid) {

    var fontid = fid.split("_")[1];

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/fonts/font/" + fontid,
        method: "DELETE",
        success: editFonts
    });
};

var updateFont = function (fid) {
    var fontid = fid.split("_")[2];
    $("#font_update_" + fontid).hide();

    var newFont = {};
    newFont.family = $("#font_family_" + fontid).val();
    newFont.category = $("#font_category_" + fontid).val();
    newFont.rule = $("#font_rule_" + fontid).val();
    newFont.url = $("#font_url_" + fontid).val();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/fonts/font/" + fontid,
        method: "PUT",
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            category: newFont.category,
            family: newFont.family,
            rule: newFont.rule,
            url: newFont.url
        }),
        success:editFonts
    });
};

var displayFonts = function (data) {
    var editableFonts = data;

    $("#current-fonts-list").children().remove();

    for (var  i = 0; i <  editableFonts.length; i++){

        var tableLeft = "<tr id=\"" + editableFonts[i]._id + "\""+ ">";
        var updateButton = "<td><button id=\"font_update_" + editableFonts[i]._id + "\""+ " class=\"btn btn-danger btn-sm\" onclick=\"updateFont(id)\">Update</button></td>";
        var tableRight = "<td><button id=\"font_" + editableFonts[i]._id + "\""+ " class=\"btn btn-danger btn-sm\" onclick=\"deleteFont(id)\">Delete</button></td></tr>";

        $("#current-fonts-list").append($(tableLeft + "<td>" +
            "<input type=\"text\" id=\"font_family_" + editableFonts[i]._id + "\""+ " class=\"font-edit-inputs\" value=" + editableFonts[i].family + "></td><td>" +
            "<input type=\"text\" id=\"font_category_" + editableFonts[i]._id + "\""+ " class=\"font-edit-inputs\" value=" + editableFonts[i].category + "></td><td>" +
            "<input type=\"text\" id=\"font_rule_" + editableFonts[i]._id + "\""+ " class=\"font-edit-inputs\" value=" + editableFonts[i].rule + "></td><td>" +
            "<input type=\"text\" id=\"font_url_" + editableFonts[i]._id + "\""+ " class=\"font-edit-inputs\" value=" + editableFonts[i].url + "></td><td>" +
            updateButton + tableRight
        ));

        $("#font_update_" + editableFonts[i]._id).hide();
    }
};

var editLevels = function () {

    editMode = "levels";
    setPage();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/levels",
        method: "GET",
        success: displayLevels
    });

};

var addLevel = function (evt) {
    evt.preventDefault();

    var newLevel = {};

    newLevel.name  = $("#inputLevelName").val();
    newLevel.guesses  = $("#inputLevelGuesses").val();
    newLevel.minLength  = $("#inputLevelMinLength").val();
    newLevel.maxLength  = $("#inputLevelMaxLength").val();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/levels/level",
        method: "POST",
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            name: newLevel.name,
            guesses: newLevel.guesses,
            maxLength: newLevel.maxLength,
            minLength: newLevel.minLength
        }),
        success:editLevels
    });
};

var deleteLevel = function (lid) {

    var levelid = lid.split("_")[1];
    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/levels/level/" + levelid,
        method: "DELETE",
        success: editLevels
    });

};

var updateLevel= function (lid) {
    var levelid = lid.split("_")[2];
    $("#level_update_" + levelid).hide();

    var newLevel = {};

    newLevel.name  = $("#level_name_" + levelid).val();
    newLevel.guesses  = $("#level_guesses_" + levelid).val();
    newLevel.minLength  = $("#level_minLength_" + levelid).val();
    newLevel.maxLength  = $("#level_maxLength_" + levelid).val();

    $.ajax({
        url: "/wordgame/api/v3/admins/metadata/levels/level/" + levelid,
        method: "PUT",
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            name: newLevel.name,
            guesses: newLevel.guesses,
            maxLength: newLevel.maxLength,
            minLength: newLevel.minLength
        }),
        success:editLevels
    });

};

var displayLevels = function (data) {
    var editableLevels = data;

    $("#current-levels-list").children().remove();

    for (var  i = 0; i <  editableLevels.length; i++){


        var tableLeft = "<tr id=\"" + editableLevels[i]._id + "\""+ ">";
        var updateButton = "<td><button id=\"level_update_" + editableLevels[i]._id + "\""+ " class=\"btn btn-danger btn-sm\" onclick=\"updateLevel(id)\">Update</button></td>";
        var tableRight = "<td><button id=\"level_" + editableLevels[i]._id + "\""+ " class=\"btn btn-info btn-sm\" onclick=\"deleteLevel(id)\">Delete</button></td></tr>";

        $("#current-levels-list").append($(tableLeft + "<td>" +
            "<input type=\"text\" id=\"level_name_" + editableLevels[i]._id + "\""+ " class=\"level-edit-inputs\" value=" + editableLevels[i].name + "></td><td>" +
            "<input type=\"number\" min=\"1\" id=\"level_guesses_" + editableLevels[i]._id + "\""+ " class=\"level-edit-inputs\" value=" + editableLevels[i].guesses + "></td><td>" +
            "<input type=\"number\" min=\"1\" id=\"level_minLength_" + editableLevels[i]._id + "\""+ " class=\"level-edit-inputs\" value=" + editableLevels[i].minLength +  "></td><td>" +
            "<input type=\"number\" min=\"1\" id=\"level_maxLength_" + editableLevels[i]._id + "\""+ " class=\"level-edit-inputs\" value=" + editableLevels[i].maxLength + "></td>" +
            updateButton + tableRight
        ));

        $("#level_update_" + editableLevels[i]._id).hide();

    }
};



var stopMetadataEdit = function (evt) {
    evt.preventDefault();

    editingMetadata = false;
    setPage();


};

/*************************************************/
/* Login */
/*************************************************/

var storeCSRF = function ( csrf ) {
    $(document).ajaxSend(function(e, xhr, opt){
        xhr.setRequestHeader('X-csrf', csrf);
    });
};

var promptUser = function (data) {
    alert(data.responseText);
};

var loginTry = function (evt) {
    evt.preventDefault();

    var username = $("#login-email").val();
    var password = $("#login-password").val();

    $.ajax({
        url: "/wordgame/api/v3/login",
        method: "POST",
        data: {"username": username, "password": password},
        success: loggingIn,
        error: promptUser
    });
};



var loggingIn = function (data, status, xhr) {
    user = data;

    if(user){
        /* getting game metadata */
        storeCSRF(xhr.getResponseHeader('X-csrf'));
        getMetaData();
        setPage();


    }
};

var loggingOut = function () {
    user = null;

    $.ajax({
        url: "/wordgame/api/v3/logout",
        method: "POST",
        success: setPage
    });
};


/*************************************************/
/* Users */
/*************************************************/

/* starts new game by changing view, and fetching word */
var newGame = function(evt){

    evt.preventDefault();

    $.ajax({
        url: "/wordgame/api/v3/" + user._id + "?level=" + level.name,
        headers: {"X-font": "" + fontFamily},
        method: "POST",
        data: colors,
        success: init
    });
};

/* initialize variables and show game view */
var init = function (data) {

    $("#game-list").slideUp();
    $("#game").show();
    $(".guess-form").show();


    updateGame(data);
};

/* updating game after a guess */
var updateGame = function (data) {

    game = data;

    $("#target-word").children().remove();

    setUp();

    /* if this is true player wins */
    if(game.status == "win"){
        $(".guess-form").hide();
        $("#game-well").css("background-image", "url(/images/winner.gif)");
    }
    /* if this is true player loses */
    else if(game.status == "loss" ){
        $(".guess-form").hide();
        $("#game-well").css("background-image", "url(/images/loser.gif)");
    }
};

/* set up game */
var setUp = function () {

    $(".guess-count").text(game.remaining + " guesses remaining.");

    for(var i = 0; i < game.view.length; i++){
        $("#target-word").append("<span class=\"hidden-letter\">" + game.view.charAt(i).toUpperCase()  + "</span>");
    }
    $("#guess-word").children().remove();
    for(var i = 0; i < game.guesses.length; i++){
        $("#guess-word").append("<span class=\"guessed-letter\">" + game.guesses.charAt(i).toUpperCase() +  "</span>");
    }

    $("#game-words").css({"color" : game.colors.textColor});
    $("#target-word").children().css({"background-color" : game.colors.wordBackground});
    $("#guess-word").children().css({"background-color" : game.colors.guessBackground});
    $("#font-picker , #game-words").css({"font-family" : game.font.family});
};

/* checks if guessed letter is part of the word */
var hangman = function (evt) {

    evt.preventDefault();

    var guessedLetter = $("#guess-input").val();
    if(guessedLetter == ""){
        return;
    }

    $("#guess-input").val("");

    /* check to see if letter was already guessed */
   if(game.guesses.includes(guessedLetter.toUpperCase()) || game.guesses.includes(guessedLetter.toLowerCase()) ){
       return;
   }

    /* display new letter guess*/
    $("#guess-word").children().css({"background-color" : game.colors.guessBackground});

    $.ajax({
        url: "/wordgame/api/v3/" + user._id  + "/" + game._id + "/guesses?guess=" + guessedLetter,
        method: "POST",
        success: updateGame
    });
};

/* ends game by returning to game-list view */
var endGame = function(){
    game = null;

    $("span").remove(".guessed-letter, .hidden-letter");
    $("#game-well").css("background-image","none");
    $("#game").hide();
    $("#game-list").slideDown();

    getGameList();
};

/* getting game list */
var getGameList = function(){

    $.ajax({
        url: "/wordgame/api/v3/" + user._id,
        method: "GET",
        success: setTable
    });
};

/* setting up table that displays all game for current session */
var setTable = function (data) {

    var sessionGames = data;

    $("#current-games-list").children().remove();

    for (var  i = 0; i <  sessionGames.length; i++){

        var answer = "";
        var tableLeft = "<tr onclick=\"retryGame(id)\" id=\"" + sessionGames[i]._id + "\""+ ">";
        var tableRight = "</tr>";

        if(sessionGames[i].status != "unfinished"){
            answer = sessionGames[i].target;
        }

        var unfinishedGuess= "";
        for(var j = 0; j < sessionGames[i].view.length; j++){
            unfinishedGuess = unfinishedGuess + "<span class=\"guessed-letter-sm mystery-letter_"  + i + "\">" + sessionGames[i].view.charAt(j).toUpperCase()  + "</span>";
        }

        $("#current-games-list").append($(tableLeft + "<td>" +
                sessionGames[i].level.name + "</td><td>" +
                unfinishedGuess + "</td><td>" +
                sessionGames[i].remaining +  "</td><td>" +
                answer +  "</td><td>" +
                sessionGames[i].status +
            "</td>" + tableRight
        ));

        $(".mystery-letter_" + i).css("font-family", sessionGames[i].font.family);
        $(".mystery-letter_" + i).css("color", sessionGames[i].colors.textColor);
        $(".mystery-letter_" + i).css("background-color", sessionGames[i].colors.wordBackground);
    }
};

/* retrying an unfinished game */
var retryGame = function (id) {

    $.ajax({
        url: "/wordgame/api/v3/" + user._id + "/" + id,
        method: "GET",
        success: init
    });
};

/* getting game metadata */
var getMetaData = function () {

    $.ajax({
        url: "/wordgame/api/v3/meta/" +  user._id,
        method: "GET",
        success: setOptions
    });
};

/* setting up game metadata */
var setOptions = function (meta) {

    font = meta.defaults.font;
    fontFamily = font.family;
    level = meta.defaults.level;
    colors = meta.defaults.colors;
    allFonts = meta.fonts;
    allLevels = meta.levels;

    /* setting all colors */
    $("#word-color").val(colors.wordBackground);
    $("#guess-color").val(colors.guessBackground);
    $("#fore-color").val(colors.textColor);  
    
    /* removing redundencies */    
    $("#level-picker").children().remove();
    $("#font-picker").children().remove();
        
    allLevels.forEach(function (level) {
        $("#level-picker").append($("<option></option>").text(level.name));
    });
    /* setting level to default */
    $("#level-picker").val(level.name);

    /* adding all fonts */
    allFonts.forEach(function (font) {
        $("meta").after($("<link rel=\"stylesheet\" type=\"text/css\"/>").attr("href", font.url));
        $("#font-picker").append($("<option></option>").text(font.family));
    });

    /* setting font to default */
    $("#font-picker , #game-words").css({"font-family" : fontFamily});
    $("#font-picker").val(fontFamily);
};

var updateDefaults = function () {
    $.ajax({
        url : "/wordgame/api/v3/" +  user._id + "/defaults",
        method: "PUT",
        processData: false,
        contentType: 'application/json',
        data:JSON.stringify({
            colors: colors,
            level: level,
            font: font
        })
    });
};

/* setting the page  */
var setPage = function () {

    $("#game").hide();
    $("#game-list").hide();
    $("#login").hide();
    $("#user-list").hide();
    $("#user").hide();
    $("#user-list").hide();
    $("#metadata").hide();
    $("#user-list").hide();
    $("#levels-edit").hide();
    $("#defaults-edit").hide();
    $("#fonts-edit").hide()

    if(user){
        if(user.role == "admin"){
            if(editingUser){
                $("#user").show();

                if(creatingUser){
                    $("#update-user-btn").hide();
                    $("#create-user-btn").show();
                } else {
                    $("#create-user-btn").hide();
                    $("#update-user-btn").show();
                }

            } else {

                if(editingMetadata) {
                    $("#metadata").show();


                    if (editMode == "levels") {
                        $("#levels-edit").show();
                    } else if(editMode == "fonts"){
                        $("#fonts-edit").show();
                    }else {
                        $("#defaults-edit").show();
                    }



                } else {
                    $("#user-list").show();
                    getUserList();
                }

            }

        } else {
            $("#game-list").show();

            if(game){
                $("#game-list").hide();
                $("#game").show();
            }else{
                getGameList();
            }
        }
    } else{
        $("#login").show();
    }
};

/* main  */
var main = function() {

    $.ajax({
        url : "/wordgame/api/v3/user",
        crossDomain: true,
        method: "GET",
        success: loggingIn,
        error: setPage
    });

    /* changing font to selected one */
    $("#font-picker").change(function(){
        fontFamily = $("#font-picker option:selected").text();
        for(var i = 0; i < allFonts.length; i++){
            if(allFonts[i].family == fontFamily){
                font = allFonts[i];
                break;
            }
        }
        $("#font-picker , #game-words").css({"font-family" : fontFamily});
        updateDefaults();
    });

    /* changing font to selected one, admin */
    $("#default-font-picker").change(function(){
        fontFamily = $("#default-font-picker option:selected").text();
        for(var i = 0; i < allFonts.length; i++){
            if(allFonts[i].family == fontFamily){
                font = allFonts[i];
                break;
            }
        }
        $("#default-font-picker ").css({"font-family" : fontFamily});

    });


    /* changing level to selected one */
    $("#level-picker").change(function(){
        var newLevel  = $("#level-picker option:selected").val();

        allLevels.forEach(function (l) {
            if(l.name == newLevel){
                level = l;
            }
        });

        updateDefaults();
    });

    /* changing level to selected one, admin*/
    $("#default-level-picker").change(function(){
        var newLevel  = $("#default-level-picker option:selected").val();

        allLevels.forEach(function (l) {
            if(l.name == newLevel){
                level = l;
            }
        });


    });

    /* changing color to selected one */
    $("#word-color").change(function(){
        colors.wordBackground = $("#word-color").val();
        updateDefaults();
    });

    /* changing color to selected one, admin */
    $("#default-word-color").change(function(){
        colors.wordBackground = $("#default-word-color").val();

    });

    $("#guess-color").change(function(){
        colors.guessBackground = $("#guess-color").val();
        updateDefaults();
    });

    $("#default-guess-color").change(function(){
        colors.guessBackground = $("#default-guess-color").val();

    });

    $("#fore-color").change(function(){
        colors.textColor = $("#fore-color").val();
        updateDefaults();
    });

    $("#default-fore-color").change(function(){
        colors.textColor = $("#default-fore-color").val();

    });
    $(".level-edit-inputs").bind("change",function(){
        alert("File is changed.");
    });


    $(".level-edit-inputs").delegate(function() {
        alert($(this).val());
    });

};

$(main);

$(document).on("change", ".level-edit-inputs", function(){
    var updateButton = "#level_update_";
    var lid = $(this).prop("id");
    lid = lid.split("_")[2];

    $(updateButton + lid).show();
});

$(document).on("change", ".font-edit-inputs", function(){
    var updateButton = "#font_update_";
    var fid = $(this).prop("id");
    fid = fid.split("_")[2];

    $(updateButton + fid).show();
});
