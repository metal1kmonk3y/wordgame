/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var mongoose = require('mongoose');

var GameSchema = mongoose.Schema({
    userId: {type: mongoose.Schema.Types.ObjectId, ref : 'User'},
    colors: {
        guessBackground: String,
        wordBackground:	String,
        textColor:	 String
    },
    level: {
        name: String,
        minLength: Number,
        maxLength: Number,
        guesses: Number
    },
    font: {
        url: String,
        rule: String,
        family: String,
        catergory: String
    },
    guesses: String,
    remaining: Number,
    status : String,
    target:	String,
    timestamp: Number,
    timeToComplete:	Number,
    view: String
});

var Game = mongoose.model('Game', GameSchema);

module.exports = Game;
