/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var mongoose = require('mongoose');

var DefaultsSchema = mongoose.Schema({
    name: String,
    colors: {
        guessBackground: String,
        wordBackground:	 String,
        textColor:	String
    },
    level: {
        name: String,
        minLength: Number,
        maxLength: Number,
        guesses: Number
    },
    font: {
        url: String,
        rule: String,
        family: String,
        catergory: String
    }
});

var Defaults = mongoose.model('Defaults', DefaultsSchema);

module.exports = Defaults;

