/*
 * CS402-HW3
 *
 * @Author Prasanna Shiwakoti
 * @Modified 3/10/2017
 */

var Game = require('./gameModel');

var createGame = function (game, cb) {
    new Game(game).save(cb);
};

var searchGameById = function( gid, cb ){
    Game.findById(gid, cb );
};

var searchGameByUserId = function( uid, cb ){
    var query = { "userId" : uid };

    Game.find( query, cb );
};

var deleteGamebyId =function (gid, cb) {
    Game.findOneAndRemove( { '_id' : gid}, cb );
};


var updateGameById = function( gid, newGame, cb ){
    Game.findOne({'_id': gid}, function (err, game) {
        if(err) {cb(err, null);
        } else{
            newGame.target = game.target;
            game = newGame;
            game.save(cb);
        }
    });
};

module.exports.createGame = createGame;
module.exports.searchGameById = searchGameById;
module.exports.updateGameById = updateGameById;
module.exports.searchGameByUserId = searchGameByUserId;
module.exports.deleteGamebyId = deleteGamebyId;