/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */
;
var User = require('./userModel');

var saveUser = function(user, cb){
    new User(user).save(cb);
};

var searchById = function ( uid, cb ) {
    User.findById(uid, cb );
};

var searchByEmail = function ( email, cb ) {
    User.findOne( { 'email' : email }, cb );
};

var searchAllUsers = function( cb ){

    User.find( { }, cb );
};

var deleteUserById = function(uid, cb){
    User.findOneAndRemove({'_id': uid}, cb);
};

var updateUserById = function (uid, newUser, cb) {
    User.findOne({'_id': uid}, function (err, user) {
        if(err) {
            cb(err, null);
        } else{
            user.name = newUser.name;
            user.email = newUser.email;
            user.role = newUser.role;
            user.enabled = newUser.enabled;

            user.save(cb);
        }
    });
};

module.exports.searchById = searchById;
module.exports.searchByEmail = searchByEmail;
module.exports.searchAllUsers = searchAllUsers;
module.exports.saveUser = saveUser;
module.exports.deleteUserById = deleteUserById;
module.exports.updateUserById = updateUserById;