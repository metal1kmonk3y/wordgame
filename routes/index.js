/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */
var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var fs = require('fs');

/* database */
var users = require('./users');
var metadb = require('./metadb');
var games = require('./games');
var worddb = fs.readFileSync('./data/wordlist.txt', 'utf8').split('\n');

var words = {};
var meta = {};

/* accept word by level */
var acceptsWord = function (level, word) {
    var wordLength = word.length;
    if(wordLength <= level.maxLength && wordLength >= level.minLength ){
        return true;
   }
};

var removeIfUnfinished = function (game) {
    if( game.status == "unfinished"){
        game.target = "";
    }
};

/* genetrating unique id
*
* @param count: number of bytes
*/
var uid =  function(count){
    return crypto.randomBytes(count).toString('hex');
};

/*************************************************/
/* ROUTES */
/*************************************************/

/* login */
router.post( '/login', function( req, res) {
    req.session.regenerate( function( err ) {
        users.searchByEmail( req.body.username, function(err, user ) {

            if(err || user == undefined) return  res.status( 403 ).send( {msg :'Error with username/password or status'});

            user.verifyPassword(req.body.password , function (err, isMatch) {
                if(err || !isMatch) return  res.status( 403).send( {msg :'Error with verification username/password or status'});

                user.password = undefined;
                req.session.user = user;

                var xcsrf = uid(64);
                req.session.xcsrf = xcsrf;
                res.set("X-csrf", xcsrf);
                res.status(200).send(user);
            });
        });
    });
});

/* logout */
router.post( '/logout', function( req, res ) {
    req.session.regenerate( function(err) {
        res.status(200).send( { msg : 'Successfully Logged out' } );
    });
});

/* user */
router.get('/user', function (req, res) {
    var user = req.session.user;

    if(user){
        var xcsrf = uid(64);
        req.session.xcsrf = xcsrf;
        res.set("X-csrf", xcsrf);
        res.status(200).send(user);
    } else {
        res.status( 403 ).send({msg : 'Forbidden' });
    }
});


/* get all available fonts */
router.get('/meta/fonts', function (req, res) {

    metadb.getAllFonts(function (err, fonts) {
        if (err) res.status(500).send({ msg : "Error: Unable to get fonts!"});
        res.status(200).send(fonts);
    });
});

/* get meta data */
router.get('/meta/:userid',function (req, res) {

    metadb.getAllLevels(function (err, levels) {
        if(err) return res.status(500).send({ msg : "Error: Unable to get levels!"});
        meta.levels = levels;

        Object.keys(meta.levels ).forEach(function (key) {
            var level = meta.levels [key];
            words[level.name] = [];
        });

        worddb.forEach(function (word) {
            word = word.trim();

            Object.keys(meta.levels).forEach(function (key) {

                var level = meta.levels[key];
                if(acceptsWord(level, word)){
                    words[level.name].push(word);
                }
            });
        });

        metadb.getAllFonts(function (err, fonts) {

            if (err) res.status(500).send({ msg : "Error: Unable to get fonts!"});
            meta.fonts = fonts;

            users.searchById(req.params.userid , function (err, user) {

                if (err) res.status(500).send({ msg : "Error: Unable to get user!"});

                meta.defaults = user.defaults;
                res.status(200).send(meta);

            });
        });
    });

});

/* authenticate */
router.all('/:userid*', function (req, res, next) {
    var authUser = req.session.user._id;
    var actualUser = req.params.userid;
    var authCsrf = req.session.xcsrf;
    var actualCsrf = req.get('X-csrf');

    if(actualUser && authUser &&  authCsrf && actualCsrf && req.session.user.enabled && authCsrf == actualCsrf && actualUser == authUser){

        next();
    }else {
        res.redirect('/');
    }
});


/* create a new game for a particular user */
router.post('/:userid',function (req, res) {
    var game = {};
    var index;

    var fontFamily = req.get("X-font");
    var font;

    game.userId = req.params.userid;
    game.colors = req.body || metadb.defaults.colors;
    game.status = "unfinished";
    game.timestamp = Date.now();
    game.timeToComplete = 0 ;
    game.target = "";

    /* getting requested level from database */
    metadb.searchLevelByName(req.query.level, function (err, level) {
        if(err) return res.status(500).send({ msg : "Error: Unable to get level while trying to create game!"});
        delete level._id;
        game.level = level;
        game.guesses = "";
        game.remaining =  level.guesses;

        /*  getting target word by appropriate level */
        index = (parseInt(uid(4) , 16)) % words[level.name].length;
        game.target = words[level.name][index];

        game.view = "";
        for(var i = 0; i < game.target.length; i++){
            game.view = game.view + "_";
        }

        metadb.searchFontByFamily(fontFamily, function (err, font) {
            if(err) return res.status(500).send({ msg : "Error: Unable to get font while trying to create game!"});
            delete font[0]._id;
            game.font = font[0];

            games.createGame(game, function (err, game) {
                if(err) return res.status(500).send({ msg : "Error: Unable to get save game while trying to create game!"});
                game.target = "";
                res.status(200).send(game);
            });
        });

    });
});

/* players guess evaluation */
router.post('/:userid/:gid/guesses',function (req, res) {
    var guess = req.query.guess;
    var gotAtLeastOne = false;
    var gid = req.params.gid;
    var userid = req.params.userid;

    games.searchGameById(gid, function (err, game) {
        if(err) return res.status(200).send({ msg : "Error: Unable to find game while trying to complete guess!"});

        /* if game is valid for user it is evaluated */
        if(req.session.user.id == game.userid) {

            game.guesses = game.guesses + guess;
            for (var i = 0; i < game.target.length; i++) {
                if (game.target.charAt(i) == guess) {

                    var temp = game.view;
                    game.view = temp.substring(0, i) + guess + temp.substring(i + 1);
                    gotAtLeastOne = true;
                }
            }

            /* determining new game status */
            if (game.target == game.view) {
                game.status = "win";
                game.timeToComplete = Date.now() - game.timestamp;

            } else if (!gotAtLeastOne) {
                game.remaining = game.remaining - 1;

                if (game.remaining == 0) {
                    game.status = "loss";
                    game.timeToComplete = Date.now() - game.timestamp;
                }
            }

            games.updateGameById(game.id, game, function (err, game) {
                if (err) return res.status(200).send({msg: "Error: Unable to find game while trying to update!"});
                removeIfUnfinished(game);

                res.status(200).send(game);
            })

        } else{
            res.status( 403 ).send({msg : 'Forbidden' });
        }
    });

});

/* get all games for a particular user */
router.get('/:userid',function (req, res) {
    var userid = req.params.userid;

    games.searchGameByUserId(userid, function (err, games) {
        if (err) res.status(500).send({ msg : "Error: Getting all games!"});
        games.forEach(function (game) {
            removeIfUnfinished(game);
        });

        res.status(200).send(games);
    });
});

/* get a particular game for a specified user */
router.get('/:userid/:gid',function (req, res) {
    var userid = req.params.userid;
    var gameid = req.params.gid;

    games.searchGameById(gameid, function (err, game) {
       if(err) res.status(500).send({ msg : "Error: Getting a particular game!"});

        removeIfUnfinished(game);

        res.status(200).send(game);
    });
});

/* update defaults for a specified user */
router.put("/:userid/defaults",function (req, res) {
    var newDefaults = req.body;
    var sessionUserID = req.params.userid;
    delete newDefaults.level._id;

    users.searchById(sessionUserID, function (err, user) {
        if (err) return res.status(200).send({msg: "Error: Unable to get user while updating user defaults!"});
        user.defaults = newDefaults;

        users.updateUserById(user.id, user , function (err, user) {
             if (err) return res.status(200).send({msg: "Error: Unable to update user defaults!"});
                    res.status(200).send(user.defaults);
        });
    });

});

module.exports = router;