/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var Font = require('./fontModel');
var Level = require('./levelModel');
var Defaults = require('./defaultsModel');

var searchFontByFamily = function( family, cb ){
    Font.find({family : family}, cb);
};

var getAllFonts = function( cb ){
    Font.find({}, cb);
};

var saveFont = function (font, cb) {
    new Font(font).save(cb);
};

var deleteFontById = function (fid, cb) {
    Font.findOneAndRemove({'_id': fid}, cb);
};

var updateFontById = function (fid, newFont, cb) {
    Font.findOne({'_id': fid},function (err, font) {
        if(err) {
            cb(err, null);
        } else{
            font.category = newFont.category;
            font.family = newFont.family;
            font.rule = newFont.rule;
            font.url = newFont.url;
            font.save(cb);
        }
    });
};

var searchLevelByName = function( level, cb ){
    Level.findOne({name: level}, cb);
};

var getAllLevels = function( cb ){
    Level.find({}, cb);
};

var saveLevel = function (level, cb) {
    new Level(level).save(cb);
};
var deleteLevelById = function (lid, cb) {
    Level.findOneAndRemove({'_id': lid}, cb);
};

var updateLevelById = function(lid, newLevel, cb){
    Level.findOne({'_id' : lid}, function (err, level) {
        if(err){
            cb(err, null);
        } else {
            level.name = newLevel.name;
            level.guesses = newLevel.guesses;
            level.minLength = newLevel.minLength;
            level.maxLength = newLevel.maxLength;

            level.save(cb);
        }
    });
};

var getDefaults = function (cb) {
    Defaults.findOne({name:"gameDefault"}, cb);
};

var setDefaults = function (newDefaults, cb) {
    Defaults.findOne({name:"gameDefault"}, function (err, defaults) {
        if(err) {
            cb(err, null);
        } else{
            defaults.colors = newDefaults.colors;
            defaults.level = newDefaults.level;
            defaults.font = newDefaults.font;

            defaults.save(cb);
        }
    });
};

module.exports.getAllFonts = getAllFonts;
module.exports.searchFontByFamily = searchFontByFamily;
module.exports.saveFont= saveFont;
module.exports.deleteFontById = deleteFontById ;
module.exports.updateFontById = updateFontById ;

module.exports.getAllLevels = getAllLevels;
module.exports.searchLevelByName = searchLevelByName;
module.exports.saveLevel = saveLevel;
module.exports.deleteLevelById = deleteLevelById;
module.exports.updateLevelById = updateLevelById ;

module.exports.getDefaults = getDefaults;
module.exports.setDefaults = setDefaults;


