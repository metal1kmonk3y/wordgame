/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var mongoose = require('mongoose');

var FontSchema = mongoose.Schema({
    category: String,
    family:	String,
    rule: String,
    url: {type: String, unique: true}
});

var Font = mongoose.model('Font', FontSchema);

module.exports = Font;

