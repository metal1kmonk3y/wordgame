/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var mongoose = require('mongoose');

var LevelSchema = mongoose.Schema({
    name: {type: String, unique: true},
    guesses: Number,
    minLength: Number,
    maxLength: Number
});

var Level = mongoose.model('Level', LevelSchema);
module.exports = Level;

