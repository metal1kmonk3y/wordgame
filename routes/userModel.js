/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var SALT_FACTOR = 10;

var UserSchema = mongoose.Schema({
    name : {
        first: String, last: String
    },
    email: {type: String, unique: true},
    password: String,
    enabled: Boolean,
    role: String,
    defaults: {
        colors: {
            guessBackground: String,
            wordBackground: String,
            textColor: String
        },
        level: {
            name: String,
            minLength: Number,
            maxLength: Number,
            guesses: Number
        },
        font: {
            url: String,
            rule: String,
            family: String,
            catergory: String
        }
    }
});

/* hashing pw before save*/
UserSchema.pre('save', function (next) {
    var user = this;
    
    // hash pw if changed
    if(!user.isModified('password')) return next();
    
    //generate a salt
    bcrypt.genSalt(SALT_FACTOR, function(err, salt){
        if(err) return next(err);        
        
        // hashing it into pieces
        bcrypt.hash(user.password, salt, function (err, hash) {
            if(err)return next(err);

            user.password = hash;
            next();

        });
    });  
});


/* match with hashed password */
UserSchema.methods.verifyPassword = function (enteredPassword, cb) {
  
    bcrypt.compare(enteredPassword, this.password, function (err, isMatch) {
        if(err){
            cb(err);
        } else {
            cb(null, isMatch);
        }
    });
};

var User = mongoose.model('User', UserSchema);

module.exports = User;