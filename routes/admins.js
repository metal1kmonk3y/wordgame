/*
 * CS402-HW4
 *
 * @Author Prasanna Shiwakoti
 * @Modified 4/3/2017
 */

var express = require('express');
var router = express.Router();

var users = require('./users');
var metadb = require('./metadb');

/*************************************************/
/* ROUTES */
/*************************************************/


/* create a new user */
router.post('/user', function (req, res) {

    var newUser = req.body;

    if(newUser.enabled == "true"){
        newUser.enabled = true;
    } else {
        newUser.enabled = false;
    }

    metadb.getDefaults(function (err, defaults) {
        if(err) res.status( 200 ).send( {msg :'Error with getting defaults'});

        defaults._id = undefined;
        defaults.name = undefined;
        
        newUser.defaults = defaults;
        
        users.saveUser(newUser, function (err, user) {
            if(err) res.status( 200 ).send( {msg :'Error with saving user during account creation'});
            res.status( 200 ).send(user);
        });

    });
});

/* update user */
router.put('/user/:userid', function (req, res) {

    users.updateUserById(req.params.userid, req.body, function (err, user) {
        if (err) return res.status(200).send({msg: "Error: Unable to get update a user!"});

        res.status(200).send(user);
    });

});



/* get a particular user  */
router.get('/user/:userid', function (req, res) {

    users.searchById(req.params.userid, function (err, user) {
        if (err) return res.status(200).send({msg: "Error: Unable to get a user in admin mode!"});
        user.password = undefined;
        res.status(200).send(user);
    })

});


/* get all users */
router.get('/users', function (req, res) {

    users.searchAllUsers(function (err, users) {
        if (err) return res.status(200).send({msg: "Error: Unable to get all users in admin mode!!"});

        users.forEach(function (user) {
            user.password = undefined;
        });

        res.status(200).send(users);
    });
});

/* get fonts data */
router.get('/metadata/fonts',function (req, res) {
    metadb.getAllFonts(function (err, fonts) {
        if (err) return res.status(200).send({msg: "Error: Unable to get all fonts in admin mode!!"});

        res.status(200).send(fonts)
    })
});

/* add a new font */
router.post('/metadata/fonts/font',function (req, res) {

    metadb.saveFont(req.body, function (err, font) {
        if (err) return res.status(200).send({msg: "Error: Unable to save a new font in admin mode!!"});

        res.status(200).send(font);
    })
});

/* delete a font  */
router.delete('/metadata/fonts/font/:fontid',function (req, res) {

    metadb.deleteFontById(req.params.fontid, function (err, font) {
        if( err ) {
            res.status( 400 ).send( { msg : err } );
        } else {
            res.status(200).send( font);
        }
    });
});

/* update font */
router.put('/metadata/fonts/font/:fontid', function (req, res) {

    metadb.updateFontById(req.params.fontid, req.body, function (err, font) {
        if (err) return res.status(200).send({msg: "Error: Unable to get update a font!"});

        res.status(200).send(font);
    });

});

/* get levels data */
router.get('/metadata/levels',function (req, res) {
    metadb.getAllLevels(function (err, levels) {
        if (err) return res.status(200).send({msg: "Error: Unable to get all levels in admin mode!!"});

        res.status(200).send(levels)
    })
});

/* add a new level */
router.post('/metadata/levels/level',function (req, res) {

    metadb.saveLevel(req.body, function (err, level) {
        if (err) return res.status(200).send({msg: "Error: Unable to save a new level in admin mode!!"});

        res.status(200).send(level);
    })
});

/* delete a level */
router.delete('/metadata/levels/level/:levelid',function (req, res) {
    metadb.deleteLevelById(req.params.levelid, function (err, level) {
        if( err ) {
            res.status( 400 ).send( { msg : err } );
        } else {
            res.status(200).send( level );
        }
    });
});

/* update level */
router.put('/metadata/levels/level/:levelid', function (req, res) {

    metadb.updateLevelById(req.params.levelid, req.body, function (err, level) {
        if (err) return res.status(200).send({msg: "Error: Unable to get update alevel!"});

        res.status(200).send(level);
    });

});


/* get defaults data */
router.get('/metadata/defaults',function (req, res) {
    var allMetas = {};

    metadb.getDefaults(function (err, defaults) {
        if (err) return res.status(200).send({msg: "Error: Unable to get all defaults in admin mode!!"});
        allMetas.defaults = defaults;
        metadb.getAllFonts(function (err, fonts) {
            if (err) return res.status(200).send({msg: "Error: Unable to get all fonts in admin mode!!"});

            allMetas.fonts = fonts;

            metadb.getAllLevels(function (err, levels) {
                if (err) return res.status(200).send({msg: "Error: Unable to get all levels in admin mode!!"});
                allMetas.levels = levels;
                res.status(200).send(allMetas);
            })
        })
    })
});


/* get defaults data */
router.put('/metadata/defaults',function (req, res) {
    var newDefaults = req.body;
    var allMetas = {};

    metadb.setDefaults(newDefaults, function (err, defaults) {
        if (err) return res.status(200).send({msg: "Error: Unable to set all defaults in admin mode!!"});

        allMetas.defaults = defaults;
        metadb.getAllFonts(function (err, fonts) {
            if (err) return res.status(200).send({msg: "Error: Unable to get all fonts in admin mode!!"});

            allMetas.fonts = fonts;

            metadb.getAllLevels(function (err, levels) {
                if (err) return res.status(200).send({msg: "Error: Unable to get all levels in admin mode!!"});
                allMetas.levels = levels;
                res.status(200).send(allMetas);
            })
        })
    })
});


module.exports = router;